
String.prototype.scan = function(regex) {
    if (!regex.global)
      throw "regex must have 'global' flag set";
    var r = []
    this.replace(regex, function(it) {
      r.push(it);
    });
    return r;
};

AL = (() => {
  const OFFSET = 9 * 60 * 60 * 1000;

  const q = location.search.substring(1);

  const Save = (page, included, excluded) => {
    const storageKey = 'save-' + page + '-' + AL.key();

    const data = {};
    const targets = [];

    for (const [name, def] of Object.entries(included)) {
      targets.push(name);
      data[name] = def;
    }

    for (const [name, def] of Object.entries(excluded)) {
      data[name] = def;
    }

    const savedRaw = localStorage.getItem(storageKey);
    if (savedRaw) {
      const saved = JSON.parse(savedRaw);
      targets.forEach((name) => {
        if (saved.hasOwnProperty(name))
          data[name] = saved[name];
      });
    }

    return {
      data,
      update: function () {
        const raw = {};
        targets.forEach((name) => {
          raw[name] = this.data[name];
        });

        localStorage.setItem(storageKey, JSON.stringify(raw));
      },
    };
  };

  const Updater = (interval, body) => {
    let handle;

    function getInterval() {
      if (typeof interval === 'function') {
        return interval();
      } else {
        return interval;
      }
    }

    async function update() {
      try {
        await body();
      } catch (e) {
        console.error(e);
      }
    }

    function start() {
      if (handle)
        clearTimeout(handle);

      handle = setTimeout(async () => {
        await update();
        start();
      }, getInterval());
    }

    start();

    return {
      restart: async (justNow) => {
        if (justNow)
          await update();
        start();
      },

      setInterval: (v) => {
        interval = v;
        start();
      },

      set: (_body) => {
        body = _body;
        start();
      }
    }
  };

  const Fetcher = (makeSince) => {
    const baseUrl = (location.host == '0.0.0.0' ? './' : '../');

    if (typeof makeSince !== 'function') {
      throw 'makeSince is not a function';
    }

    let sinceIds = new Map();

    return {
      reset: () => {
        sinceIds.clear();
      },

      fetch: async (name) => {
        const atFirst = !sinceIds.has(name);

        let url = baseUrl + name + '.json?';
        if (atFirst) {
          url += 'since=' + makeSince();
        } else {
          url += 'since_id=' + (sinceIds.get(name) + 1);
        }

        const result = await fetch(url).then((it) => it.json());

        if (result.length <= 0) {
          if (atFirst) {
            sinceIds.set(
              name,
              await fetch(baseUrl + '!/last_id.json').then((it) => it.json())
            );
          }
        } else {
          let sinceId = -1;
          result.forEach((it) => {
            if (sinceId < it.id)
              sinceId = it.id;
          });
          sinceIds.set(name, sinceId);
        }

        return result;
      }
    }
  };

  function maybeNumber(v) {
    const n = parseFloat(v);
    return isNaN(v) ? v : n;
  }

  function parseDateTime(text) {
    const utc = new Date(text).getTime();
    return {
      utc,
      text,
      local: utc + OFFSET
    };
  }

  function zeroPad(v, n) {
    v = v.toString();
    for (; v.length < n;) {
      v = '0' + v;
    }
    return v;
  }

  return {
    Fetcher,

    UpdateInterval: 5 * 1000,

    Save,

    Updater,

    elapsedTimeText: (dt) => {
      const now = new Date().getTime();
      let d = (now - dt) / 1000;

      function two(a, au, b, bu) {
        a = parseInt(a, 10) + au;
        b = parseInt(b, 10);
        if (0 < b)
          return a + b + bu;
        return a;
      }

      if (d < 60)
        return parseInt(d, 10) + 's';
      let p = d % 60;
      d /= 60;
      if (d < 60)
        return two(d, 'm', p, 's');
      p = d % 60;
      d /= 60;
      if (d < 24)
        return two(d, 'h', p, 'm');
      p = d % 24;
      d /= 24;
      return two(d, 'd', p, 'h');
    },

    ensure: (f) => {
      return () => {
        try {
          return f();
        } catch(e) {
          console.error(e);
        }
      };
    },

    jump: (n) => {
      if (location.pathname.replace(/.*\//g, '') == n)
        return 'self';
      return './' + n + location.search;
    },

    parameters: () => {
      const result = {};

      q.split('&').forEach((pair) => {
        const m = pair.match(/([^=]*)=(.*)/);
        if (m) {
          result[m[1]] = m[2];
        } else {
          result[pair] = true;
        }
      });

      return result;
    },

    targetNames: () => {
      const target = AL.parameters()['target'];
      return target ? target.split(',') : [];
    },

    key: () => {
      return AL.targetNames().sort().join(',');
    },

    parseDateTime,

    scanData: (item, rootName, multi) => {
      function genName(n) {
        return multi ? rootName + '-' + n : n;
      }

      function indexedName(n, i) {
        return 0 == i ? n : n + '-' + i;
      }

      const created_at = parseDateTime(item.created_at);
      const items = [];
      let ni = 0;

      item.data.split(/[,&\s]/).forEach((it) => {
        const m = it.match(/([^:=]+)[:=](.+)/);
        if (m) {
          items.push({
            name: genName(m[1]),
            created_at,
            data: maybeNumber(m[2])
          });
        } else {
          const n = parseFloat(it);
          if (isNaN(n)) {
            items.push({
              name: genName(it),
              created_at,
              data: '😺'
            });
          } else {
            items.push({
              name: indexedName(rootName, ni),
              created_at,
              data: n,
            });
            ni++;
          }
        }
      });

      return items;
    },

    scanNumbers: (item, rootName, multi) => {
      return AL.scanData(item, rootName, multi).filter((it) => (typeof it.data === 'number'));
    },

    shortenDateTime: (dt) => {
      const t = new Date(AL.parseDateTime(dt).local);
      const now = new Date();
      let skipped = true;
      let result = '';

      function add(prop, offset, suffix, n) {
        const v = t['get' + prop]();
        if (skipped && (v == now['get' + prop]())) {
          //
        } else{
          result += zeroPad(v + offset, n) + suffix;
          skipped = false;
        }
      }

      add('Year', 1900, '-', 4);
      add('Month', 1, '-', 2);
      add('Date', 0, 'T', 2);
      add('Hours', 0, ':', 2);
      add('Minutes', 0, ':', 2);
      add('Seconds', 0, '', 2);

      return result;
    },

  }

})();


use std::path::Path;
use std::sync::Arc;
use std::time::{SystemTime, Duration};

use chrono::{DateTime, Utc};
use r2d2_sqlite::SqliteConnectionManager;
use rusqlite::{NO_PARAMS, ToSql};
use serde_derive::*;
use url::percent_encoding::percent_decode;

use crate::errors::{AppResult, AppResultU};
use crate::types::*;



#[derive(Clone)]
pub struct Database {
    conn: Arc<r2d2::Pool<SqliteConnectionManager>>,
}

#[derive(Deserialize)]
pub struct Param {
    limit: Option<usize>,
    since: Option<String>,
    since_id: Option<i64>,
}


type QResult<T> = Result<T, rusqlite::Error>;


impl Database {
    pub fn make<T: AsRef<Path>>(db: &T) -> AppResult<Self> {
        let manager = r2d2_sqlite::SqliteConnectionManager::file(db.as_ref());
        let pool = r2d2::Pool::builder()
            .max_size(15)
            .build(manager)
            .unwrap();
        Ok(Self { conn: Arc::new(pool) })
    }

    pub fn connect(&self) -> AppResult<r2d2::PooledConnection<SqliteConnectionManager>> {
        Ok(self.conn.get()?)
    }

    pub fn initialize(&self) -> AppResultU {
        let conn = self.connect()?;

        let mut stmt = conn.prepare("SELECT name FROM sqlite_master WHERE type='table' AND name='logs'")?;
        let mut rows = stmt.query(NO_PARAMS)?;
        if rows.next()?.is_none() {
            conn.execute("CREATE TABLE logs (name varchar(80), data varchar, created_at varchar(19) default (datetime()));", NO_PARAMS)?;
            conn.execute("CREATE INDEX name_index ON logs (name);", NO_PARAMS)?;
        }

        Ok(())
    }

    pub fn append(&self, name: &str, data: &str) -> AppResult<String> {
        let conn = self.connect()?;

        let created_at: DateTime<Utc> = Utc::now();
        let created_at = created_at.format("%Y-%m-%d %H:%M:%S").to_string();

        let data =  percent_decode(data.as_bytes()).decode_utf8()?;
        let data = data.into_owned();
        let args = &[&name as &dyn ToSql, &data as &dyn ToSql, &created_at as &dyn ToSql];
        conn.execute("INSERT INTO logs (name, data, created_at) VALUES ($1, $2, $3)", args)?;

        Ok(created_at)
    }


    pub fn get_logs(&self, name: &str, param: &Param, reverse: bool) -> AppResult<Entries> {
        let conn = self.connect()?;
        let mut args = vec![name];

        let mut select = "SELECT rowid, data, datetime(created_at, 'localtime') FROM logs WHERE name = $1".to_owned();

        let since = param.since.as_ref().map(String::as_ref).map(parse_since);

        if let Some(since_id) = param.since_id {
            // FIXME
            select.push_str(&format!(" and {} <= rowid", since_id));
        }

        if let Some(since) = &since {
            args.push(since);
            select.push_str(" and $2 <= created_at");
        }

        select.push_str(" ORDER BY created_at DESC");

        if let Some(limit) = param.limit {
            // FIXME
            select.push_str(&format!(" LIMIT {}", limit));
        }

        let mut stmt = conn.prepare(&select)?;
        let iter = stmt.query_map(args, |row| {
            Ok(Entry {
                id: row.get(0)?,
                data: row.get(1)?,
                created_at: row.get(2)?,
            })
        })?;

        let mut es = iter.collect::<QResult<Vec<Entry>>>()?;

        if !reverse {
            es.reverse();
        }

        Ok(Entries {
            name: name.to_string(),
            entries: es
        })
    }

    pub fn get_groups(&self) -> AppResult<Vec<Group>> {
        let conn = self.connect()?;

        let select = "SELECT name, MAX(created_at) FROM logs GROUP BY name";

        let mut stmt = conn.prepare(select)?;

        let iter = stmt.query_map(NO_PARAMS, |row| {
            Ok(Group {
                name: row.get(0)?,
                last_created_at: row.get(1)?
            })
        })?;

       Ok(iter.collect::<QResult<Vec<Group>>>()?)
    }

    pub fn get_last_id(&self) -> AppResult<i64> {
        let conn = self.connect()?;

        let select = "SELECT rowid FROM logs ORDER BY rowid DESC LIMIT 1";

        let mut stmt = conn.prepare(select)?;

        let mut iter = stmt.query_map(NO_PARAMS, |row| {
            row.get(0)
        })?;

        Ok(iter.next().transpose()?.unwrap_or(-1))
    }
}

fn parse_since(s: &str) -> String {
    if let Ok(t) = s.parse::<u64>() {
        let t = Duration::from_secs(t);
        let t: DateTime<Utc> = DateTime::from(SystemTime::UNIX_EPOCH + t);
        dbg!(t.format("%Y-%m-%d %H:%M:%S").to_string())
    } else {
        s.to_owned()
    }
}

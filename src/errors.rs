
use actix_web::{HttpResponse, ResponseError};
use failure::Fail;



pub type AppResult<T> = Result<T, AppError>;
pub type AppResultU = Result<(), AppError>;



#[derive(Fail, Debug)]
pub enum AppError {
    #[fail(display = "Invalid log name")]
    InvalidLogName,
    #[fail(display = "IO Error: {}", 0)]
    Io(std::io::Error),
    #[fail(display = "Invalid number: {}", 0)]
    NumberFormat(std::num::ParseIntError),
    #[fail(display = "SQLite error: {}", 0)]
    Sqlite(rusqlite::Error),
    #[fail(display = "R2D2 Error: {}", 0)]
    R2d2(r2d2::Error),
    #[fail(display = "UTF-8 Error: {}", 0)]
    Utf8(std::str::Utf8Error),
}

impl ResponseError for AppError {
    fn error_response(&self) -> HttpResponse {
        HttpResponse::BadRequest()
            .body(self.to_string())
    }
}



macro_rules! define_error {
    ($source:ty, $kind:ident) => {
        impl From<$source> for AppError {
            fn from(error: $source) -> AppError {
                AppError::$kind(error)
            }
        }
    }
}

define_error!(r2d2::Error, R2d2);
define_error!(rusqlite::Error, Sqlite);
define_error!(std::io::Error, Io);
define_error!(std::num::ParseIntError, NumberFormat);
define_error!(std::str::Utf8Error, Utf8);

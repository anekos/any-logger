
use std::path::Path;

//http, HttpResponse,
use actix_web::{App,  HttpServer, Responder, web, HttpResponse};
use lazy_static::*;
use log::info;
use regex::Regex;

use crate::db::{Database, Param};
use crate::errors::{AppError, AppResultU};
use crate::hook::Hook;



#[derive(Clone)]
struct State {
    database: Database,
    hook: Option<Hook>,
}

pub fn start<T: AsRef<Path>>(address: &str, port: u16, hook: Option<Hook>, db: &T) -> AppResultU {
    let database = Database::make(db)?;
    database.initialize()?;

    let state = State { database, hook };

    let server = HttpServer::new(move || {
        let state = state.clone();
        App::new()
            .data(state)
            .route("/", web::get().to(on_index))
            .route("/!/al.css", web::get().to(on_alcss))
            .route("/!/al.js", web::get().to(on_aljs))
            .route("/!/chart", web::get().to(on_chart))
            .route("/!/count", web::get().to(on_count))
            .route("/!/groups.json", web::get().to(on_groups_json))
            .route("/!/last_id.json", web::get().to(on_last_id_json))
            .route("/!/latest", web::get().to(on_latest))
            .route("/!/log", web::get().to(on_log))
            .route("/{name}.json", web::get().to(on_logs_json))
            .route("/{name}", web::get().to(on_logs))
            .route("/{name}/{data}", web::get().to(on_append))
    });

    server
        .bind(format!("{}:{}", address, port))?
        .run()?;

    Ok(())
}

fn on_index() -> impl Responder {
    HttpResponse::Ok()
        .content_type("text/html")
        .body(include_str!("../html/index.html"))
}

fn on_groups_json(state: web::Data<State>) -> impl Responder {
    info!("groups");

    state.database.get_groups().map(web::Json)
}

fn on_aljs() -> impl Responder {
    HttpResponse::Ok()
        .content_type("application/javascript")
        .body(include_str!("../html/al.js"))
}

fn on_alcss() -> impl Responder {
    HttpResponse::Ok()
        .content_type("text/css")
        .body(include_str!("../html/al.css"))
}

fn on_chart() -> impl Responder {
    HttpResponse::Ok()
        .content_type("text/html")
        .body(include_str!("../html/chart.html"))
}

fn on_count() -> impl Responder {
    HttpResponse::Ok()
        .content_type("text/html")
        .body(include_str!("../html/count.html"))
}

fn on_latest() -> impl Responder {
    HttpResponse::Ok()
        .content_type("text/html")
        .body(include_str!("../html/latest.html"))
}

fn on_log() -> impl Responder {
    HttpResponse::Ok()
        .content_type("text/html")
        .body(include_str!("../html/log.html"))
}

fn on_last_id_json(state: web::Data<State>) -> impl Responder {
    state.database.get_last_id().map(web::Json)
}

fn on_logs(state: web::Data<State>, name: web::Path<String>, query: web::Query<Param>) -> impl Responder {
    info!("logs: name = {}", name);
    validate_log_name(&*name)?;

    state.database.get_logs(&*name, &query, true).map(|it| {
        let html = it.to_string();

        HttpResponse::Ok()
            .content_type("text/plain")
            .body(html)
    })
}

fn on_logs_json(state: web::Data<State>, name: web::Path<String>, query: web::Query<Param>) -> impl Responder {
    info!("logs: name = {}", name);
    validate_log_name(&*name)?;

    state.database.get_logs(&*name, &query, false).map(|it| web::Json(it.entries))
}

fn on_append(state: web::Data<State>, pair: web::Path<(String, String)>) -> impl Responder {
    let (name, data) = &*pair;
    info!("append: name = {}, data = {}", name, data);
    validate_log_name(&*name)?;

    match state.database.append(name, data) {
        Ok(created_at) => {
            if let Some(hook) = &state.hook {
                hook.on_log(created_at, name.to_owned(), data.to_owned())?;
            }
            Ok("OK")
        },
        Err(err) => Err(err)
    }
}

fn validate_log_name(name: &str) -> AppResultU {
    lazy_static! {
        static ref RE: Regex = Regex::new(r"\A[-_.a-zA-Z0-9]+\z").unwrap();
    }
    if RE.is_match(name) {
        Ok(())
    } else {
        Err(AppError::InvalidLogName)
    }
}

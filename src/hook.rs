
use std::path::PathBuf;
use std::thread::spawn;
use std::process::{Command, ExitStatus};

use crate::errors::{AppResult, AppResultU};


#[derive(Clone, Debug)]
pub struct Hook {
    script_path: PathBuf,
}


impl Hook {
    pub fn new(script_path: PathBuf) -> Self {
        Hook { script_path }
    }

    pub fn on_log(&self, created_at: String, name: String, data: String) -> AppResultU {
        let script_path = self.script_path.clone();
        spawn(move || {
            run_script(script_path, created_at, "log", name, data);
        });
        Ok(())
    }
}

fn run_script(script_path: PathBuf, at: String, on_what: &'static str, name: String, data: String) {
    let mut cmd = Command::new(script_path);
    cmd.arg(at);
    cmd.arg(on_what);
    cmd.arg(name);
    cmd.arg(data);

    match spawn_command(cmd) {
        Err(err) =>
            eprintln!("Hook script failed: {}", err),
        Ok(status) =>
            println!("Hook script finished: {}", status),
    }
}

fn spawn_command(mut cmd: Command) -> AppResult<ExitStatus> {
    let mut child = cmd.spawn()?;
    Ok(child.wait()?)
}

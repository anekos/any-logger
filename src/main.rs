
use std::path::{Path, PathBuf};

use argparse::{ArgumentParser, Store, StoreOption};

mod db;
mod errors;
mod hook;
mod server;
mod types;

use errors::AppResultU;
use hook::Hook;



fn app() -> AppResultU {
    env_logger::init();

    let mut port = 3000;
    let mut address = "0.0.0.0".to_owned();
    let mut hook_script: Option<PathBuf> = None;
    let mut db: Option<PathBuf> = None;

    {
        let mut ap = ArgumentParser::new();
        ap.refer(&mut port).add_option(&["-p", "--port"], Store, "Port number");
        ap.refer(&mut address).add_option(&["-a", "--address"], Store, "Listen address");
        ap.refer(&mut hook_script).add_option(&["-h", "--hook"], StoreOption, "Hook script");
        ap.refer(&mut db).add_option(&["-d", "--db"], StoreOption, "DB file");
        ap.parse_args_or_exit();
    }

    let hook = hook_script.map(Hook::new);

    println!("Listening on http://{}:{}/", address, port);
    if let Some(hook) = &hook {
        println!("with a hook script ({:?})", hook);
    }
    let db = db.take().unwrap_or(Path::new("logs.db").to_path_buf());
    server::start(&address, port, hook, &db)?;

    Ok(())
}

fn main() {
    match app() {
        Ok(()) => (),
        Err(err) => {
            eprintln!("{}", err);
            std::process::exit(1);
        },
    }
}

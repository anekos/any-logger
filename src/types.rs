
use std::fmt;

use serde_derive::*;


#[derive(Serialize)]
pub struct Entry {
    pub id: i64,
    pub data: String,
    pub created_at: String
}

pub struct Entries {
    pub name: String,
    pub entries: Vec<Entry>
}

#[derive(Serialize)]
pub struct Group {
    pub name: String,
    pub last_created_at: String
}


impl fmt::Display for Entry {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "[{}] {}", self.created_at, self.data)
    }
}

impl fmt::Display for Entries {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut buf = String::new();

        buf.push_str(&self.name);
        buf.push_str("\n");

        for entry in &self.entries {
            buf.push_str(&entry.to_string());
            buf.push_str("\n");
        }

        write!(f, "{}", buf)
    }
}
